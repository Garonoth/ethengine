from engine.characters import Person, EditStates
#from names.generator import NameGenerator
from game.metadata import CharacterMetadata
from game.scene import Scene, default_scenes
from game.combat import Battle

parties = {}
#namegen = NameGenerator('names/start.txt', 'names/mid.txt', 'names/end.txt')

class Party(object):
    def __init__(self, pcs, scene):
        self.pcs = pcs
        self.current_scene = scene
        self.save_slot = -1
        self.current_battle = None
        self.current_event = None
        self.current_action = ''
        self.events = []

    def next_event(self):
        if len(self.events) > 0:
            self.current_event = self.events[0]
            self.current_action = self.current_event.action
            self.events = self.events[1:]

def new_party(key):
    parties[key] = Party([Person(key)], default_scenes['dummy'])

    for p in parties[key].pcs:
        p.editable = EditStates.FULL

        p.metadata = CharacterMetadata()
        p.metadata.portrait = 'firestorm.jpg'

def get_party(key):
    return parties[key]
