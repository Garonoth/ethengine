from enum import Enum
import random

from engine.conflicts import CombatActions, CombatProfile
from engine.characters import Person

class Outcome(Enum):
    UNDECIDED = 1
    VICTORY = 2
    DEFEAT = 3

class Battle(object):
    def __init__(self):
        self.combatants = []
        self.allowed_actions = {}
        self.acting_index = -1
        self.ai_target_index = 0
        self.ai_action = None
        self.next_command = 'turn'
        self.outcome = Outcome.UNDECIDED

    def get_acting_character(self):
        return self.combatants[self.acting_index]

    def get_allowed_actions(self, char, scope, is_reaction):
        results = []

        for name in self.allowed_actions:
            action_type = self.allowed_actions[name].action

            if is_reaction:
                if action_type == CombatActions.PREPARE or action_type == CombatActions.RECOVER:
                    continue
            elif action_type == CombatActions.BLOCK:
                continue

            if action_type == CombatActions.RECOVER:
                resistance = self.allowed_actions[name].target_resistance
                if char.res(resistance).get_penalty() <= 0:
                    continue #only allow recovery if in penalty range

            action_scope = self.allowed_actions[name].scope
            if scope == 'all' or scope == 'combat' or scope == action_scope or action_scope == 'all':
                results.append(name)

        return results

    def _get_action_for_ai(self, char, scope, is_reaction):
        min_weight = 5
        keys = self.get_allowed_actions(char, scope, is_reaction)
        weights = []
        for key in keys:
            if self.allowed_actions[key].action == CombatActions.BLOCK:
                weights.append(char.get(char.find_defense_for_block(scope)) + min_weight)
            else:
                weights.append(char.get(self.allowed_actions[key].skill) + min_weight)

        print('Choices: '+str(keys))
        print('Weights: '+str(weights))
        return random.choices(keys, weights=weights)[0]

    def _get_target_for_ai(self, char):
        if char.combat_status.last_target_index >= 0 and random.randrange(2) == 0:
            return char.combat_status.last_target_index

        my_team = char.combat_status.team
        choices = [x for x in self.combatants if x.combat_status.team != my_team]
        target = random.choice(choices)
        return self.combatants.index(target)

    def prepare_ai_move(self):
        char = self.get_acting_character()
        self.ai_target_index = self._get_target_for_ai(char)
        self.ai_action = self._get_action_for_ai(char, 'all', False)

    def ai_action_category(self):
        if self.ai_action is None:
            return 'NONE'
        return self.allowed_actions[self.ai_action].action.name

    def ai_action_scope(self):
        if self.ai_action is None:
            return 'all'
        return self.allowed_actions[self.ai_action].scope

    def ai_action_target(self):
        return self.combatants[self.ai_target_index]

    def check_outcome(self):
        pcs_alive = False
        enemies_alive = False
        for char in self.combatants:
            if char.is_defeated():
                continue
            if char.combat_status.team == 0:
                pcs_alive = True
            else:
                enemies_alive = True

        if not pcs_alive:
            return Outcome.DEFEAT
        elif not enemies_alive:
            return Outcome.VICTORY
        else:
            return Outcome.UNDECIDED

    def roll_initiative(self):
        for char in self.combatants:
            char.roll_initiative()
            char.last_target_index = -1
            char.combat_penalty = 0

        self.combatants.sort(key=lambda x: x.combat_status.initiative)
        self.combatants.reverse()

    def next_turn(self, force=False):
        if self.next_command != 'turn' and not force:
            return

        self.outcome = self.check_outcome()
        if self.outcome != Outcome.UNDECIDED:
            self.next_command = 'end'
            return

        self._advance_index()

        while self.get_acting_character().is_defeated():
            self._advance_index()

        self.next_command = 'action'

    def _advance_index(self):
        self.acting_index += 1
        if self.acting_index >= len(self.combatants):
            self.acting_index = 0

    def resolve_action(self, action, target_index, force=False):
        if self.next_command != 'action' and not force:
            if self.next_command == 'end':
                return {'log': ['Battle has ended.']}
            else:
                return {'log': []}

        attacker = self.combatants[self.acting_index]

        if self.get_acting_character().combat_status.team == 0: #player decides attacker side
            allowed = self.get_allowed_actions(self.get_acting_character(), 'all', False)
            if not action in allowed:
                action = 'Break Defenses'

            defender = self.combatants[target_index]
            attacker_action = self.allowed_actions[action]
            defender_action = self.allowed_actions[self._get_action_for_ai(defender, self.allowed_actions[action].scope, True)]
            attacker.combat_status.last_target_index = target_index #remember chosen target
        else: #player decides defender side
            allowed = self.get_allowed_actions(self.combatants[self.ai_target_index], self.allowed_actions[self.ai_action].scope, True)
            if not action in allowed:
                action = 'Full Defense'

            defender = self.combatants[self.ai_target_index]
            attacker_action = self.allowed_actions[self.ai_action]
            defender_action = self.allowed_actions[action]
            attacker.combat_status.last_target_index = self.ai_target_index

        if attacker_action.action == CombatActions.BLOCK:
            attacker_action.defense = attacker.find_defense_for_block(defender_action.scope)
        else:
            attacker_action.defense = attacker.find_defense_for_attack(attacker_action.skill)

        if defender_action.action == CombatActions.BLOCK:
            defender_action.defense = defender.find_defense_for_block(attacker_action.scope)
        else:
            defender_action.defense = defender.find_defense_for_attack(defender_action.skill)

        log = []

        if attacker_action.action == CombatActions.PREPARE:
            bonus = attacker.prepare_action(attacker_action.skill)
            if attacker_action.success is not None:
                log.append(attacker_action.success(attacker, defender, attacker_action, defender_action, bonus));
            expected = 1
        elif attacker_action.action == CombatActions.RECOVER:
            healed = attacker.recover_action(attacker_action.skill, attacker_action.target_resistance, attacker_action.severity)
            if attacker_action.success is not None:
                log.append(attacker_action.success(attacker, defender, attacker_action, defender_action, healed));
            expected = 1
        else:
            expected = 2

        if attacker.combat_status.weakened:
            attacker.combat_status.weakened = False #reset
        elif attacker.combat_status.combat_penalty > 0:
            attacker.combat_status.combat_penalty = 0 #survived the round, recover
            log.append(attacker.name+' recovers their footing.')
            expected += 1

        if attacker_action.action != CombatActions.PREPARE and attacker_action.action != CombatActions.RECOVER:
            attacker_rolls = attacker.fight(attacker_action, defender_action)
            defender_rolls = defender.fight(defender_action, attacker_action)

            if attacker_action.action == CombatActions.BLOCK:
                attacker_result = attacker_rolls[1]
            else:
                attacker_result = attacker_rolls[0]

            if defender_action.action == CombatActions.BLOCK:
                defender_result = defender_rolls[1]
            else:
                defender_result = defender_rolls[0]

            log.append('%s: %s (%s) vs %s: %s (%s)' % (attacker.name, attacker_action.name, str(attacker_result), defender.name, defender_action.name, str(defender_result)))

            log.extend(self._resolve(attacker, defender, attacker_action, defender_action, attacker_rolls[0] - defender_rolls[1]))
            log.extend(self._resolve(defender, attacker, defender_action, attacker_action, defender_rolls[0] - attacker_rolls[1]))

        if len(log) < expected:
            log.append(attacker.name+' and '+defender.name+' clash without result.')

        self.next_command = 'turn'
        return {'log': log}

    def _resolve(self, attacker, defender, attacker_action, defender_action, amount):
        log = []

        if attacker_action.action == CombatActions.WEAKEN:
            max_amount = defender.combat_status.max_combat_penalty - defender.combat_status.combat_penalty
            if max_amount <= 0:
                attacker_action = self.allowed_actions['Strike to kill'] #ToDo: refactor to data-driven
            elif amount > max_amount:
                amount = max_amount

        if amount > 0:
            if attacker_action.action == CombatActions.WEAKEN:
                amount = defender.take_combat_penalty(defender_action.skill, amount) #also sets weakened
            elif attacker_action.action == CombatActions.HURT or attacker_action.action == CombatActions.RECKLESS:
                defender.take_damage(attacker_action.target_resistance, amount, attacker.attack_severity(attacker_action.skill))

            if attacker_action.success is not None:
                log.append(attacker_action.success(attacker, defender, attacker_action, defender_action, amount));
        return log

    def default_actions(self):
        self.add_action(CombatProfile('Break defenses', CombatActions.WEAKEN, 'Combat', None, '', '', 'physical', [Battle.success_weaken, None, None]))
        self.add_action(CombatProfile('Strike to kill', CombatActions.HURT, 'Combat', None, 'Toughness', 'L', 'physical', [Battle.success_hurt, None, None]))
        self.add_action(CombatProfile('Trip', CombatActions.HURT, 'Combat', None, 'Agility', 'L', 'physical', [Battle.success_trip, None, None]))
        self.add_action(CombatProfile('Reckless Attack', CombatActions.RECKLESS, 'Combat', None, 'Toughness', 'L', 'physical', [Battle.success_reckless, None, None]))
        self.add_action(CombatProfile('Full Defense', CombatActions.BLOCK, 'Combat', None, '', '', 'all', [Battle.success_block, None, None]))
        self.add_action(CombatProfile('Shoot', CombatActions.RECKLESS, 'Shooting', None, 'Toughness', 'L', 'physical', [Battle.success_shoot, None, None]))
        self.add_action(CombatProfile('Aim', CombatActions.PREPARE, 'Shooting', None, '', '', 'physical', [Battle.success_aim, None, None]))
        self.add_action(CombatProfile('Intimidate', CombatActions.RECKLESS, 'Intimidation', None, 'Courage', 'L', 'mental', [Battle.success_intimidate, None, None]))
        self.add_action(CombatProfile('Confuse', CombatActions.RECKLESS, 'Subterfuge', None, 'Intuition', 'L', 'mental', [Battle.success_confuse, None, None]))
        self.add_action(CombatProfile('Hide', CombatActions.PREPARE, 'Stealth', None, '', '', 'physical', [Battle.success_hide, None, None]))
        self.add_action(CombatProfile('Cast Magic', CombatActions.PREPARE, 'Spellcasting', None, '', '', 'mental', [Battle.success_magic, None, None])) #TODO: use essence
        self.add_action(CombatProfile('Get Up', CombatActions.RECOVER, 'Acrobatics', None, 'Agility', 'L', 'physical', [Battle.success_stand, None, None]))

    def add_action(self, action):
        self.allowed_actions[action.name] = action

    @staticmethod
    def success_weaken(attacker, defender, attacker_action, defender_action, amount):
        return defender.name+' loses '+str(amount)+' points of footing.'

    @staticmethod
    def success_hurt(attacker, defender, attacker_action, defender_action, amount):
        return defender.name+' takes '+str(amount)+' Toughness damage.'

    @staticmethod
    def success_trip(attacker, defender, attacker_action, defender_action, amount):
        return defender.name+' takes '+str(amount)+' Agility damage.'

    @staticmethod
    def success_reckless(attacker, defender, attacker_action, defender_action, amount):
        return defender.name+' takes '+str(amount)+' Toughness damage.'

    @staticmethod
    def success_block(attacker, defender, attacker_action, defender_action, amount):
        return attacker.name+' successfully blocks.' #Shouldn't happen

    @staticmethod
    def success_shoot(attacker, defender, attacker_action, defender_action, amount):
        return defender.name+' takes '+str(amount)+' Toughness damage.'

    @staticmethod
    def success_aim(attacker, defender, attacker_action, defender_action, amount):
        return attacker.name+' gains a bonus of '+str(amount)+' for aiming.'

    @staticmethod
    def success_intimidate(attacker, defender, attacker_action, defender_action, amount):
        return defender.name+' takes '+str(amount)+' Courage damage.'

    @staticmethod
    def success_confuse(attacker, defender, attacker_action, defender_action, amount):
        return defender.name+' takes '+str(amount)+' Intuition damage.'

    @staticmethod
    def success_hide(attacker, defender, attacker_action, defender_action, amount):
        return attacker.name+' gains a bonus of '+str(amount)+' for hiding.'

    @staticmethod
    def success_magic(attacker, defender, attacker_action, defender_action, amount):
        return attacker.name+' gains a bonus of '+str(amount)+' for casting magic.'

    @staticmethod
    def success_stand(attacker, defender, attacker_action, defender_action, amount):
        return attacker.name+' recovers '+str(amount)+' Agility.'
