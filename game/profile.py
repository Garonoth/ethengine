import os

import json
import jsonpickle

from engine.characters import Person
from game.metadata import CharacterMetadata
import game.party as party
from game.scene import Scene, default_scenes
from game.events import Event
import game.items #Test

profiles = {}

class Profile(object):
	def __init__(self, name, slots):
		self.name = name
		self.password = ''
		self.slots = slots
		self.chargen_step = 0

def get_profile_path(key):
	return 'userdata/profiles/'+key+'.usr'

def get_save_path(key, slot):
	return 'userdata/saves/'+key+'-'+slot+'.sav'

def get_profile(key):
	if key in profiles.keys():
		return profiles[key]
	else:
		try:
			with open(get_profile_path(key), 'r') as f:
				profiles[key] = jsonpickle.decode(f.read());
			return profiles[key]
		except IOError:
			return None

def new_profile(key):
	if key in profiles.keys():
		raise KeyError('User already exists')

	profiles[key] = Profile(key, [])
	with open(get_profile_path(key), 'w') as f:
		f.write(jsonpickle.encode(profiles[key]))

def save_profile(key):
	with open(get_profile_path(key), 'w') as f:
		f.write(jsonpickle.encode(profiles[key]))

def save_game(key, slot):
	party.parties[key].save_slot = slot
	with open(get_save_path(key, slot), 'w') as f:
		f.write(json.dumps(encode_party(party.parties[key])))

	if not slot in profiles[key].slots:
		profiles[key].slots.append(slot)

def load_game(key, slot):
	with open(get_save_path(key, slot), 'r') as f:
		party.parties[key] = decode_party(json.loads(f.read()));
		party.parties[key].save_slot = slot

def delete_game(key, slot):
	os.remove(get_save_path(key, slot))
	profiles[key].slots.remove(slot)

### Encoding and decoding ###
def encode_stat_values(collection):
	values = {}
	for k in collection.keys():
		stat = collection[k]
		values[stat.name] = stat.base

	return values

def encode_skills(collection):
	values = {}
	for k in collection.keys():
		skill = collection[k]
		values[skill.name] = {'value': skill.base, 'modstat': skill.mod_stat.name}

	return values

def encode_aspects(collection):
	values = []
	for k in collection.keys():
		aspect = collection[k]
		values.append({'name': aspect.name, 'conviction': str(aspect.conviction)})

	return values

def encode_character(char):
	encoded = {}

	encoded['name'] = char.name
	meta = {'portrait': char.metadata.portrait,
			'bio': char.metadata.bio,
			'age': char.metadata.age,
			'gender': char.metadata.gender,
			'race': char.metadata.race,
			'hair': char.metadata.hair,
			'eyes': char.metadata.eyes,
			'calling': char.metadata.calling
		}
	encoded['metadata'] = meta

	encoded['abilities'] = encode_stat_values(char.abilities)
	encoded['resistances'] = encode_stat_values(char.resistances)
	encoded['experience'] = encode_stat_values(char.experience)
	encoded['needs'] = encode_stat_values(char.needs)

	encoded['skills'] = encode_skills(char.skills)
	encoded['aspects'] = encode_aspects(char.aspects)
	#encoded['nimbus'] = encode_statset(char.nimbus)

	encoded['basic_exp'] = {
	'exp_collected': char.exp_collected,
	'exp_required': char.exp_required,
	'exp_step': char.exp_step
	}

	return encoded

def encode_party(party):
	encoded = {}

	encoded['serial_version'] = 1 #In case we need to patch something gameplay related later

	encoded['current_scene'] = party.current_scene.name
	pcs = []
	for pc in party.pcs:
		pcs.append(encode_character(pc))

	encoded['pcs'] = pcs

	return encoded


def decode_stat_values(source, dest):
	for k in source.keys():
		dest[k].set(source[k])

def decode_skills(source, dest, mod_stats):
	for k in source.keys():
		dest[k].mod_stat = mod_stats[source[k]['modstat']]
		dest[k].set(source[k]['value'])

def decode_character(encoded):
	name = encoded['name']
	level = encoded['experience']['Level']

	char = Person(name, level)

	meta = CharacterMetadata()
	meta.portrait = encoded['metadata']['portrait']
	meta.bio = encoded['metadata']['bio']
	meta.age = encoded['metadata']['age']
	meta.gender = encoded['metadata']['gender']
	meta.race = encoded['metadata']['race']
	meta.hair = encoded['metadata']['hair']
	meta.eyes = encoded['metadata']['eyes']
	meta.calling = encoded['metadata']['calling']
	char.metadata = meta

	decode_stat_values(encoded['abilities'], char.abilities)
	decode_stat_values(encoded['resistances'], char.resistances)
	decode_stat_values(encoded['experience'], char.experience)
	decode_stat_values(encoded['needs'], char.needs)

	decode_skills(encoded['skills'], char.skills, char.abilities)

	if 'nimbus' in encoded:
		pass #ToDo

	if 'aspects' in encoded:
		for a in encoded['aspects']:
			char.add_aspect(a['name'], a['conviction'])

	if	'basic_exp' in encoded:
		char.exp_collected = encoded['basic_exp']['exp_collected']
		char.exp_required = encoded['basic_exp']['exp_required']
		char.exp_step = encoded['basic_exp']['exp_step']

	###Test###
	sword = game.items.generate_item({'item': 'Sword', 'quality': 1})
	char.inventory.add(sword)
	char.inventory.equip(sword)
	#char.inventory.remove(sword)

	#for item in char.inventory.items:
	#	print(item)
	#print(char.inventory.tools)

	char.recalc()
	return char

def decode_party(encoded):
	scene_key = encoded['current_scene']
	if scene_key in default_scenes:
		scene = default_scenes[scene_key]
	else:
		scene = default_scenes['dummy']

	pcs = []
	for pc in encoded['pcs']:
		pcs.append(decode_character(pc))

	result = party.Party(pcs, scene)
	#result.events.append(Event('random_encounter'))

	return result
