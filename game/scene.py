from game.regions import get_region
from game.events import Event

class Scene(object):
    def __init__(self, name, bgimage, region):
        self.name = name
        self.bgimage = bgimage
        self.region = region
        self.options = [] #events

default_scenes = {'dummy': Scene('dummy', 'village2.jpg', get_region('Haunted Forest'))}
default_scenes['dummy'].options.append(Event('random_encounter', 'Fight something'))
default_scenes['dummy'].options.append(Event('hunt', 'Hunt for food'))
default_scenes['dummy'].options.append(Event('forage', 'Forage for food'))
default_scenes['dummy'].options.append(Event('rest', 'Rest for the day'))
