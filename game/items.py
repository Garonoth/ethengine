import json
import random
from copy import deepcopy
from glob import glob

from engine.inventory import ItemCategories, Item, Tool, Consumable, Wearable
from engine.effects import get_effect

base_items = {}
items_ready = False

def add_base_item(i):
    global items_ready
    global base_items

    if not items_ready:
        load_base_items()

    base_items[i.birth_name] = i

def get_base_item(name):
    global items_ready
    global base_items

    if not items_ready:
        load_base_items()

    return base_items[name]

def copy_item(item):
    return deepcopy(item)

def generate_item(template):
    if 'quality' in template:
        q = template['quality']
    elif 'min_quality' in template:
        q = random.randint(template['min_quality'], template['max_quality'])
    elif 'q' in template: #shorthand
        q = template['q']
    else:
        q = 0

    if 'stack_size' in template:
        stack_size = template['stack_size']
    elif 'min_stack' in template:
        stack_size = random.randint(template['min_stack'], template['max_stack'])
    else:
        stack_size = 1

    item = copy_item(get_base_item(template['item']))

    if q < item.min_quality:
        item.quality = item.min_quality
    else:
        item.quality = q

    item.stack_size = stack_size
    item.base_item = template['item']

    #ToDo: attribute overrides and unique flag

    item.calc_value()

    return item

def load_base_items():
    global items_ready
    global base_items

    items_ready = True
    base_items = {}

    _load_from_directory('gamedata/items/*/')

def _load_from_directory(path):
    print('Items: scanning: '+path+'*.json')

    for f in glob(path+'*.json', recursive=True):
        _load_item(f)

def _load_item(path):
    print('Items: loading '+path)

    with open(path, 'r') as f:
        t = json.load(f)

    name = t['name']
    category = ItemCategories[t['category'].upper()]
    weight = t['weight']
    base_value = t['base_value']
    description = t['description']
    icon = t['icon']

    if 'min_quality' in t:
        min_quality = t['min_quality']
    else:
        min_quality = -5

    if 'damage_level' in t:
        damage_level = t['damage_level']
    else:
        damage_level = 'L'

    if 'spoiling_quality' in t:
        spoiling_quality = t['spoiling_quality']
    else:
        spoiling_quality = 0

    effects = []
    for name in t['effects']:
        effects.append(get_effect(name))

    item_type = t['type']

    if 'quality_levels' in t:
        quality_levels = t['quality_levels']
    else:
        quality_levels = None

    if item_type.lower() == 'consumable':
        item = Consumable(name, category, weight=weight, base_value=base_value, effects=effects, min_quality=min_quality, \
        description=description, icon=icon, quality_levels=quality_levels)
    elif item_type.lower() == 'tool':
        item = Tool(name, category, t['skills'], damage_level=damage_level, weight=weight, base_value=base_value, effects=effects, min_quality=min_quality, \
        description=description, icon=icon, quality_levels=quality_levels)
    elif item_type.lower() == 'wearable':
        areas = []
        for name in t['areas']:
            areas.append(WearableAreas[name.upper()])

        item = Wearable(name, category, areas, weight=weight, base_value=base_value, effects=effects, min_quality=min_quality, \
        description=description, icon=icon, quality_levels=quality_levels)
    else:
        item = Item(name, category, weight=weight, base_value=base_value, effects=effects, min_quality=min_quality, \
        description=description, icon=icon, quality_levels=quality_levels)

    item.spoiling_quality = spoiling_quality

    add_base_item(item)
