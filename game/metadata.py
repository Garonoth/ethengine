class CharacterMetadata(object):
    def __init__(self):
        self.portrait = None
        self.bio = None

        self.age = 18
        self.gender = ''
        self.race = ''
        self.hair = ''
        self.eyes = ''
        self.calling = ''
