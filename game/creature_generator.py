import json
import random

from engine.characters import Person
from engine.aspects import get_aspect
from names.generator import NameGenerator
from game.metadata import CharacterMetadata
from game.items import generate_item

templates = {}
namegen = NameGenerator('gamedata/names/start.txt', 'gamedata/names/mid.txt', 'gamedata/names/end.txt')

def load_template(name):
    global templates

    with open('gamedata/creatures/'+name+'.json', 'r') as f:
        templates[name] = json.load(f)

def randomize(val, randomness):
    return val + random.randint(0, randomness) - random.randint(0, randomness) #roll 2 dice for "inverse V" distribution

def generate(template_name):
    global templates
    global namegen

    if not template_name in templates:
        load_template(template_name)

    t = templates[template_name]
    variant = random.choice(t['variants'])
    randomness = t['random-range']

    if t['generate-name']:
        name = namegen.generate()
    else:
        name = variant['name']

    npc = Person(name=name, level=t['level'], race=variant['race'])

    npc.metadata = CharacterMetadata()
    npc.metadata.portrait = variant['portrait']

    for skill in npc.skills:
        npc.skills[skill].set(t['skill-default']);

    for name in t['stats']:
        npc.set(name, randomize(t['stats'][name], randomness))

    for name in t['skills']:
        skill = npc.skl(name)
        skill.mod_stat = npc.abl(t['skills'][name]['stat'])
        skill.set(randomize(t['skills'][name]['value'], randomness))

    for name in t['aspects']:
        npc.add_aspect(name, None, recalc=False)

    if 'resources' in t:
        for item in t['resources']:
            if (not ('chance' in item)) or item['chance'] >= random.randint(0, 100):
                npc.resources.add(generate_item(item))

    if 'inventory' in t:
        for item in t['inventory']:
            if (not ('chance' in item)) or item['chance'] >= random.randint(0, 100):
                item_obj = generate_item(item)
                npc.inventory.add(item_obj)
                if item_obj.skills != None:
                    npc.inventory.equip(item_obj)

    npc.recalc()

    return npc
