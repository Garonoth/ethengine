import os
import json
import random
from glob import glob

import game.creature_generator as npcgen

regions = {}
regions_ready = False

class Encounter(object):
    def __init__(self, creature_pool, min_foes, max_foes):
        self.creature_pool = creature_pool
        self.min_foes = min_foes
        self.max_foes = max_foes

    def generate(self):
        foes = []
        for x in range(0, random.randint(self.min_foes, self.max_foes)):
            template = random.choice(self.creature_pool)
            foes.append(npcgen.generate(template))

        return foes

class Region(object):
    def __init__(self, name, comfort_level, encounters, prey):
        self.name = name
        self.comfort_level = comfort_level
        self.encounters = encounters
        self.prey = prey

    def random_encounter(self):
        return random.choice(self.encounters)

def get_region(name):
    global regions_ready
    global regions

    if not regions_ready:
        load_generic_regions()

    return regions[name]

def add_region(r):
    global regions_ready
    global regions

    if not regions_ready:
        load_generic_regions()

    regions[r.name] = r

def load_generic_regions():
    global regions_ready
    global regions

    regions_ready = True
    regions = {}

    _load_from_directory('gamedata/regions/generic/')

def _load_from_directory(path):
    print('Regions: scanning: '+path+'*.json')

    for f in glob(path+'*.json', recursive=True):
        _load_region(f)

def _load_region(path):
    print('Regions: loading '+path)

    with open(path, 'r') as f:
        t = json.load(f)

    name = t['name']
    comfort_level = t['comfort_level']

    encounters = []
    for e in t['encounters']:
        encounters.append(Encounter(e['creature_pool'], e['min_foes'], e['max_foes']))

    prey = t['prey']

    add_region(Region(name, comfort_level, encounters, prey))
