from engine.characters import Person

if __name__ == "__main__": 
    x = Person('NPC')

    x.resistances['Toughness'].damage(25, 'L')
    x.set('Climbing', 5)
    x.set('Strength', 18)
    print('Strength: '+str(x.get('Strength')))
    print('Climbing: '+str(x.get('Climbing')))
    
    for s in x.abl(): #shorthand for x.abilities.values()
        print(str(s))
    for s in x.res():
        print(str(s))
    for s in x.skl():
        print(str(s))

    print('Toughness penalty: '+str(x.stat('Toughness').get_penalty()))
    print('Total penalty: '+str(x.get_penalties('physical')))

    for i in range(0, 5):
        print('Climbing roll: '+str(x.roll('Climbing', 'physical')))
