import random

class NameGenerator(object):
    
    def __init__(self, start_path, mid_path, end_path):
        self.start = self.read_list(start_path)
        self.middle = self.read_list(mid_path)
        self.end = self.read_list(end_path)

        self.mid_chance = 0.5

    @staticmethod
    def read_list(path):
        result = []
        with open(path, 'r') as f:
            for line in f:
                result.append(line.strip())

        return result

    def generate(self):
        name = random.choice(self.start)

        if random.random() <= self.mid_chance:
            name = name + random.choice(self.middle)

        name = name + random.choice(self.end)

        return name
