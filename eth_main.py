import sys
import traceback
import os
import json
from flask import Flask, render_template, session, request, redirect, url_for, jsonify, Response

import engine.codex as codex
import engine.characters as characters
import game.party as party
import game.profile as profile
import game.combat as combat
import game.metadata as metadata
import game.creature_generator as npc_generator
import game.items as items
from game.scene import Scene

NO_LOGIN_REQUIRED = ['/login', '/header', '/register', '/error/profile']
Flask.secret_key = os.urandom(24)
app = Flask(__name__, static_url_path='/static', static_folder='web/static', template_folder='web/templates')

### Web UI ###

main_scene = Scene('main_menu', 'village1.jpg', None)

@app.before_request
def session_mgmt():
    session['id'] = 'Shadow' #DEBUG: for testing
    session.permanent = True

@app.before_request
def fetch_profile():
    if not (request.endpoint == 'static' or request.path in NO_LOGIN_REQUIRED):
        if profile.get_profile(get_user()) is None:
            return redirect(url_for('err_no_profile'))

        try:
            party.get_party(get_user())
        except KeyError:
            party.new_party(get_user())

@app.route('/', methods=['GET', 'POST'])
def show_hello():
    key = get_user()
    errors = []
    profile.get_profile(key).chargen_step = 0 #restart character creation

    if request.method == 'POST':
        slot = request.form['slot']
        if is_valid_username(slot):
            try:
                profile.save_game(key, slot)
                profile.save_profile(key)
            except IOError:
                errors.append('Could not save game state.')
        else:
            errors.append('Saved game names have to be 2 to 30 characters long and may only contain letters and numbers.')

    return render_template('hello.html', scene=main_scene, user=profile.get_profile(key), errors=errors)

@app.route('/load/<string:slot>')
def show_load_game(slot):
    key = get_user()
    errors = []

    if is_valid_username(slot):
        try:
            profile.load_game(key, slot)
        except IOError:
            errors.append('This is not a valid save game.')
    else:
        errors.append('This is not a valid save game.')

    return render_template('hello.html', scene=main_scene, user=profile.get_profile(key), errors=errors)

@app.route('/delete/<string:slot>')
def show_delete_game(slot):
    key = get_user()
    errors = []

    if is_valid_username(slot):
        try:
            profile.delete_game(key, slot)
            profile.save_profile(key)
        except IOError:
            errors.append('This is not a valid save game.')
    else:
        errors.append('This is not a valid save game.')

    return render_template('hello.html', scene=main_scene, user=profile.get_profile(key), errors=errors)

@app.route('/header')
def show_header():
    return render_template('header.html')

@app.route('/register', methods=['GET', 'POST'])
def show_register():
    if request.method == 'GET':
        return render_template('account/register.html', errors=[])

    username = request.form['user']
    if is_valid_username(username):
        profile.new_profile(username)
        return redirect(url_for('show_hello'))
    else:
        return render_template('account/register.html', errors=['Not a valid username. Names have to be 2 to 30 characters long and may only contain letters and numbers.'])

@app.route('/new', methods=['GET', 'POST'])
def show_chargen():
    user = profile.get_profile(get_user())
    if user.chargen_step == 0:
        party.new_party(get_user())
    pc = party.get_party(get_user()).pcs[0]

    (success, errors) = chargen_callbacks[user.chargen_step](pc, request)
    if success:
        user.chargen_step += 1

    if user.chargen_step < len(chargen_callbacks):
        return render_template(chargen_templates[user.chargen_step], scene=main_scene, pc=pc, codex=codex, errors=errors)
    else:
        return redirect(url_for('show_character', index=1))

@app.route('/party')
def show_party():
    player_party = party.get_party(get_user())
    return render_template('party.html', party=player_party, scene=player_party.current_scene)

### Error pages ###

@app.route('/error/profile')
def err_no_profile():
    return render_template('error/no_profile.html')

### Validation ###

def is_valid_username(name):
    return name.isalnum() and len(name) >= 2 and len(name) <= 30;

def get_user():
    return session['id']

def filter_dict(data, allowed, prefix = None):
    result = {}

    for key in data.keys():
        if prefix is not None:
            if not key.startswith(prefix):
                continue
            new_key = key.replace(prefix, '', 1)
        else:
            new_key = key

        if new_key in allowed:
            result[new_key] = data[key]

    return result

def underscore_if_empty(val):
    if val is None or len(val) < 1:
        return '_'

    return val

### Character creation ###

def chargen_abilities(pc, request):
    if request.method == 'POST':
        try:
            pc.batch_set(filter_dict(request.form, pc.abilities.keys()), pc.get('Abilities'), 5)
            pc.set('Abilities', 0)
            return (True, [])
        except ValueError as e:
            return(False, [str(e)])

    return (False, [])

def chargen_skills(pc, request):
    if request.method == 'POST':
        try:
            pc.batch_assign_modstats(filter_dict(request.form, pc.skills.keys(), 'mod_stat.'))
            pc.batch_set(filter_dict(request.form, pc.skills.keys()), pc.get('Skills'), 1)
            pc.set('Skills', 0)
            return (True, [])
        except ValueError as e:
            return(False, [str(e)])

    return (False, [])

def chargen_needs(pc, request):
    if request.method == 'POST':
        try:
            pc.metadata.age = int(request.form['age'])
            pc.name = underscore_if_empty(request.form['name'])
            pc.metadata.gender = underscore_if_empty(request.form['gender'])
            pc.metadata.race = underscore_if_empty(request.form['race'])
            pc.metadata.hair = underscore_if_empty(request.form['hair'])
            pc.metadata.eyes = underscore_if_empty(request.form['eyes'])
            pc.metadata.calling = underscore_if_empty(request.form['calling'])

            pc.batch_set(filter_dict(request.form, pc.needs.keys()))
            return (True, [])
        except ValueError as e:
            return(False, [str(e)])

    return (False, [])

chargen_callbacks = [chargen_abilities, chargen_skills, chargen_needs]
chargen_templates = ['chargen/abilities.html', 'chargen/skills.html', 'chargen/needs.html']

### Character sheet ###

@app.route('/character/<int:index>')
def show_character(index):
    player_party = party.get_party(get_user())
    pc = player_party.pcs[index-1]
    return render_template('character.html', pc=pc, scene=player_party.current_scene, id=index)

@app.route('/character/<int:index>/train/<string:stat>')
def level_character(index, stat):
    player_party = party.get_party(get_user())
    pc = player_party.pcs[index-1]
    pc.increase_stat(stat)
    return render_template('character.html', pc=pc, scene=player_party.current_scene, id=index)


### Scenes ###

@app.route('/play')
def show_main_screen():
    player_party = party.get_party(get_user())

    if 'action' in request.args: #only do user action if it's allowed
        print('Action: '+request.args['action'])

        player_party.current_action = ''
        for o in player_party.current_scene.options:
            if o.action == request.args['action']:
                player_party.current_action = o.action

    if player_party.current_action == 'battle' and player_party.current_battle.outcome == combat.Outcome.VICTORY:
        player_party.current_action = '' #ToDo: loot/aftercare screen

    if player_party.current_action == 'battle' and player_party.current_battle.outcome == combat.Outcome.DEFEAT:
        player_party.current_action = 'defeated'

    if player_party.current_action == '':
        player_party.next_event() #get next action if there is one

    if player_party.current_action == 'random_encounter':
        return new_combat()
    elif player_party.current_action == 'battle':
        return show_combat()
    elif player_party.current_action == 'defeated':
        return show_defeated()
    else:
        return show_location()

def show_location():
    player_party = party.get_party(get_user())

    return render_template('scenes/location.html', scene=player_party.current_scene)

def show_defeated():
    player_party = party.get_party(get_user())

    return render_template('scenes/location.html', scene=player_party.current_scene) #For now

#@app.route('/combat')
def show_combat():
    player_party = party.get_party(get_user())
    pc = player_party.pcs[0]

    return render_template('scenes/combat.html', scene=player_party.current_scene, battle=player_party.current_battle)

#@app.route('/combat/new')
def new_combat():
    player_party = party.get_party(get_user())

    foes = player_party.current_scene.region.random_encounter().generate()

    for foe in foes:
        foe.combat_status.team = 1

    battle = combat.Battle()
    battle.default_actions()
    battle.combatants = player_party.pcs+foes
    battle.roll_initiative()

    player_party.current_battle = battle
    player_party.current_action = 'battle'

    for char in battle.combatants:
        print(char.name+':')
        for item in char.inventory.items:
            print(item)
        print(char.inventory.tools)

    return show_combat()#redirect(url_for('show_combat'))

### AJAX backend ###

@app.route('/ajax/combat/turn', methods=['GET'])
def ajax_combat_turn():
    player_party = party.get_party(get_user())
    battle = player_party.current_battle
    battle.next_turn()
    acting_team = battle.get_acting_character().combat_status.team

    result = {
    'acting_index': battle.acting_index,
    'acting_team': acting_team,
    'log': []
    }

    result['outcome'] = battle.outcome.name

    if battle.outcome == combat.Outcome.DEFEAT:
        result['prompt'] = 'You have been defeated.'
    elif battle.outcome == combat.Outcome.VICTORY:
        result['prompt'] = 'You have been victorious.'
    elif acting_team == 0:
        result['prompt'] = 'It\'s your turn.'
        result['allowed_actions'] = battle.get_allowed_actions(battle.get_acting_character(), 'all', False)
    else:
        battle.prepare_ai_move()
        action = battle.ai_action_category()
        result['action'] = action
        if action == 'PREPARE' or action == 'RECOVER':
            result['prompt'] = battle.get_acting_character().name+' uses '+str(battle.ai_action)+'. You can\'t react.'
        else:
            result['prompt'] = battle.get_acting_character().name+' uses '+str(battle.ai_action)+'. How do you react?'
        result['target_index'] = battle.ai_target_index

        result['allowed_actions'] = battle.get_allowed_actions(battle.ai_action_target(), battle.ai_action_scope(), True)


    result['last_target_index'] = battle.get_acting_character().combat_status.last_target_index

    return jsonify(result)

@app.route('/ajax/combat/target', methods=['POST'])
def ajax_combat_target():
    data = json.loads(request.get_data())

    player_party = party.get_party(get_user())
    battle = player_party.current_battle
    try:
        index = data['index']
        char = battle.combatants[index]
    except KeyError as e:
        print('KeyError: ', e)
        return Response('{"error": "Index out of range"}', status=400, mimetype='application/json')

    return jsonify(_encode_character(char))

@app.route('/ajax/combat/action', methods=['POST'])
def ajax_combat_action():
    data = json.loads(request.get_data())

    player_party = party.get_party(get_user())
    battle = player_party.current_battle
    index = data['index']
    action = data['action']

    try:
        result = battle.resolve_action(action, index)
    except KeyError as e:
        print('KeyError: ', e)
        return Response('{"error": "Index out of range"}', status=400, mimetype='application/json')

    return jsonify(result)

def _encode_character(char):
    encoded = {}
    encoded['name'] = char.name

    resistances = {}
    for res in char.resistances.values():
        resistances[res.name] = {
        'name': res.name,
        'value': res.value,
        'damage': res.get_damage()
        }

    encoded['resistances'] = resistances
    encoded['penalty'] = char.combat_status.combat_penalty
    encoded['defeated'] = char.is_defeated()

    return encoded

### Dev server ###

if __name__ == '__main__':
    app.run()
