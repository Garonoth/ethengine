$( document ).ready(function() {
    eth_globals.validation.modsValid = false; //ToDo: remove if using pre-filled mappings
    $("#nextStep").prop("disabled", true);
});

//update modifier
$( "select" ).bind("change", function() {
	var skill = $(this).attr('name').replace("mod_stat.", "");

	var stat = $(this).val();
	if (stat.length < 1)
		return;
	
	var mod = $("#"+stat).val();
	$("[id='modifier_"+skill+"']").text(mod);
});

//validate
$( "select" ).bind("change", function() {
    var uses = {};
    var max = 3;
    var valid = true;

    $( "select" ).each(function() {
        $(this).removeClass('error');

        var key = $(this).val();

        if (key.length < 1)
            valid = false;
        else
        {
            if (key in uses)
            {
                uses[key] += 1;

                if (uses[key] > max)
                {
                    $(this).addClass('error');
                    valid = false;
                }
            }
            else
            {
                uses[key] = 1;
            }
        }
    });

    eth_globals.validation.modsValid = valid;

    if (eth_globals.isSheetValid())
        $("#nextStep").prop("disabled", false);
    else
        $("#nextStep").prop("disabled", true);
});
