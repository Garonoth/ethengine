function eth_globals_def() {
	var self = this;

	self.validation = {
		pointsValid: true,
		modsValid: true
	}

	self.isSheetValid = function() {
		return self.validation.pointsValid && self.validation.modsValid;
	}

	return self;
};

var eth_globals = eth_globals_def();

hilight = function(x)
{
	$(x).addClass("hilight");
}

unHilight = function(x)
{
	$(x).removeClass("hilight");
}

hide = function(x)
{
	$(x).addClass("hidden");
}

show = function(x)
{
	$(x).removeClass("hidden");
}
