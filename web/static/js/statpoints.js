$("input[type=number]").bind("keyup input change", function() {
  var total = 0;
  var max = parseInt($("#maxPoints").val());

  $( "input[type=number]" ).each(function() {
    total += parseInt($(this).val());
  });

  //console.log('total: '+total);

  var remaining = max - total;
  if (remaining >= 0) {
    $("#totalCounter").text('Remaining points: '+remaining);
	eth_globals.validation.pointsValid = true;
  }
  else {
    $("#totalCounter").text('You are '+(total-max)+' points above the limit!');
	eth_globals.validation.pointsValid = false;
  }
  
  if (eth_globals.isSheetValid())
	$("#nextStep").prop("disabled", false);
  else
	$("#nextStep").prop("disabled", true);
});
