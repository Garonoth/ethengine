playerInfo = function(index) {
    $.ajax({
        url: 'ajax/combat/target',
        method: 'POST',

        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },

        //processData: false,
        //dataType: 'json',
        data: JSON.stringify({"index": index}),

        //error: self.ajaxFailure,

        success: function( result ) {
            $("#playerWindow #name").text(result.name);
            updatePenalty("#playerWindow", result.penalty);
            updateHealthBar("#playerWindow", result.resistances.Toughness);
            updateHealthBar("#playerWindow", result.resistances.Agility);
            updateHealthBar("#playerWindow", result.resistances.Courage);
            updateHealthBar("#playerWindow", result.resistances.Intuition);
            updateOverlays(index, result.defeated);
        }
    });
}

attackerInfo = function(index, advanceTurn=false) {
    $.ajax({
        url: 'ajax/combat/target',
        method: 'POST',

        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },

        //processData: false,
        //dataType: 'json',
        data: JSON.stringify({"index": index}),

        //error: self.ajaxFailure,

        success: function( result ) {
            $("#targetWindow #name").text(result.name);
            updatePenalty("#targetWindow", result.penalty);
            updateHealthBar("#targetWindow", result.resistances.Toughness);
            updateHealthBar("#targetWindow", result.resistances.Agility);
            updateHealthBar("#targetWindow", result.resistances.Courage);
            updateHealthBar("#targetWindow", result.resistances.Intuition);
            updateOverlays(index, result.defeated);

            if (advanceTurn)
                nextTurn();
        }
    });
}

nextTurn = function() {
    $.ajax({
        url: 'ajax/combat/turn',
        method: 'GET',

        headers: {
            'Accept': 'application/json'
        },

        success: function( result ) {
            updateChat(result.log);
            updateActive(result.acting_index);

            eth_globals.combatInfo.defending = result.acting_team != 0;
            $("#actionPrompt").text(result.prompt);

            if (eth_globals.combatInfo.defending) {
                if (result.action == "PREPARE")
                    hideActions();
                else {
                    showActions();
                    updateActions(result.allowed_actions);
                }

                $("#init"+eth_globals.combatInfo.lastTarget).removeClass("targeted")
                $("#init"+result.target_index).addClass("targeted")
                eth_globals.combatInfo.lastTarget = result.target_index;
                eth_globals.combatInfo.target = {'index': result.acting_index};
                playerInfo(result.target_index);
                attackerInfo(result.acting_index);
            }
            else {
                showActions();
                updateActions(result.allowed_actions);

                selectTarget(result.last_target_index, true);
                playerInfo(result.acting_index);
            }

            if (result.outcome == "VICTORY" || result.outcome == "DEFEAT") {
                eth_globals.combatInfo.$resultLabel.text(result.prompt);
                eth_globals.combatInfo.$resultDialog.dialog("open");
            }
        }
    });
}

selectTarget = function(index, override=false) {
    if (eth_globals.combatInfo.defending && !override)
        return; //can't override AI targets

    if (index == eth_globals.combatInfo.lastActive)
        return; //don't select yourself

    $("#init"+eth_globals.combatInfo.lastTarget).removeClass("targeted")
    $.ajax({
        url: 'ajax/combat/target',
        method: 'POST',

        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },

        //processData: false,
        //dataType: 'json',
        data: JSON.stringify({"index": index}),

        //error: self.ajaxFailure,

        success: function( result ) {
            eth_globals.combatInfo.target = result;
            eth_globals.combatInfo.target.index = index;
            eth_globals.combatInfo.lastTarget = index;
            $("#init"+eth_globals.combatInfo.lastTarget).addClass("targeted")
            $("#targetWindow #name").text(result.name);
            updatePenalty("#targetWindow", result.penalty);
            updateHealthBar("#targetWindow", result.resistances.Toughness);
            updateHealthBar("#targetWindow", result.resistances.Agility);
            updateHealthBar("#targetWindow", result.resistances.Courage);
            updateHealthBar("#targetWindow", result.resistances.Intuition);
            updateOverlays(index, result.defeated);
        }
    });
}

executeAction = function() {
    var action = $("#action").val();
    var target = eth_globals.combatInfo.target.index;
    eth_globals.combatInfo.lastAction = action;
    $.ajax({
        url: 'ajax/combat/action',
        method: 'POST',

        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },

        data: JSON.stringify({
            "action": action,
            "index": target}),

        success: function( result ) {
            updateChat(result.log);
            attackerInfo(target, true); //refresh and then go to next turn
        }
    });
}

updateChat = function(log) {
    $chat = $("#chatWindow");

    for (var i = 0, len = log.length; i < len; i++)
        $chat.append(log[i]+"<br/>");

    $chat.scrollTop($chat.prop('scrollHeight'));
}

updateActive = function(index) {
    $("#init"+eth_globals.combatInfo.lastActive).removeClass("active")
    eth_globals.combatInfo.lastActive = index;
    $("#init"+eth_globals.combatInfo.lastActive).addClass("active")
    $("#init"+eth_globals.combatInfo.lastActive).removeClass("targeted")
}

updateHealthBar = function(root, res) {
  var ratio = (res.value - res.damage) / res.value;
  $(root+" #"+res.name+" #healthBar").css("width", (ratio*100)+"%");
}

updatePenalty = function(root, pen) {
    if (pen <= 0)
        $(root+" #penalty").text("");
    else
        $(root+" #penalty").text("Penalty: -"+pen);
}

updateOverlays = function(index, defeated) {
    var $overlay = $("#init"+index+" #defeatedOverlay");

    if (defeated)
        $overlay.removeClass("hidden");
    else
        $overlay.addClass("hidden");
}

hideActions = function() {
    eth_globals.combatInfo.$actionLabel.addClass("hidden");
    eth_globals.combatInfo.$actionPicker.addClass("hidden");
}
showActions = function() {
    eth_globals.combatInfo.$actionLabel.removeClass("hidden");
    eth_globals.combatInfo.$actionPicker.removeClass("hidden");
}
updateActions = function(options) {
    $opts = eth_globals.combatInfo.$actionPicker;

    $('option', $opts).remove();
    $.each(options, function() {
        if (this == eth_globals.combatInfo.lastAction)
            $opts.append($("<option selected />").val(this).text(this));
        else
            $opts.append($("<option />").val(this).text(this));
    });
}

initSimpleDialog = function(x) {
	x.dialog({
        modal: true,
		autoOpen: false,
		width: 400,
		buttons: [
			{
				text: "Ok",
				click: function() {
					$( this ).dialog( "close" );
                    location.reload(true);
				}
			}
		]
	});
}

$( document ).ready(function() {
    eth_globals.combatInfo = {
      'acting': null,
      'target': null,
      'lastTarget': 0,
      'lastActive': 0,
      'defending': false,
      'lastAction': '',
      $actionPicker: $('#action'),
      $actionLabel: $('#actionLabel'),
      $resultDialog: $( "#resultDialog" ),
      $resultLabel: $( "#resultMessage" )
    }

    initSimpleDialog(eth_globals.combatInfo.$resultDialog);

    nextTurn();
});
