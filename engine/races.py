races_ready = False
races = {}

class Race(object):
    def __init__(self, name, descriptors, aspects):
        self.name = name
        self.descriptors = descriptors
        self.aspects = aspects

def add_race(e):
    global races_ready
    global races

    if not races_ready:
        init_races()

    races[e.name] = e

def get_race(name):
    global races_ready
    global races

    if not races_ready:
        init_races()

    return races[name]

def init_races():
    global races_ready
    global races

    races_ready = True
    races = {}

    add_race(Race('Zkhagim',
    ['human', 'zkhagim'],
    []))

    add_race(Race('Noddin',
    ['human', 'noddin'],
    []))

    add_race(Race('Meldoin',
    ['human', 'meldoin'],
    []))

    add_race(Race('Eastling',
    ['human', 'eastling'],
    []))

    add_race(Race('Tainted',
    ['human', 'tainted'],
    []))
