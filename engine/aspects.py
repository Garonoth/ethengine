from engine.stats import Aspect
from engine.effects import get_effect

aspects_ready = False
aspects = {}

def add_aspect(a):
    global aspects_ready
    global aspects

    if not aspects_ready:
        init_aspects()

    aspects[a.name] = a

def get_aspect(name):
    global aspects_ready
    global aspects

    if not aspects_ready:
        init_aspects()

    return aspects[name]

def init_aspects():
    global aspects_ready
    global aspects

    aspects_ready = True
    aspects = {}

    add_aspect(Aspect('Warrior', 5, [get_effect('Physical Defense')]))
    add_aspect(Aspect('Insightful', 5, [get_effect('Social Preparation Bonus'), get_effect('Mental Defense')]))
    add_aspect(Aspect('Fearless', 4, [get_effect('Fearless')]))
    add_aspect(Aspect('Tough', 4, [get_effect('Tough')]))
    add_aspect(Aspect('Clearheaded', 4, [get_effect('Clearheaded')]))
    add_aspect(Aspect('Nimble', 4, [get_effect('Nimble')]))
    add_aspect(Aspect('Intuitive Magic', 5, [get_effect('Magic Preparation Bonus')]))
    add_aspect(Aspect('Rogue', 6, [get_effect('Rogue Preparation Bonus')]))
    add_aspect(Aspect('Stoic', 3, [get_effect('Block Defense')]))
    add_aspect(Aspect('Fast Healing', 9, [get_effect('Double Healing')]))
