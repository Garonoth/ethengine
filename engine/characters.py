from enum import Enum
import math

from engine.stats import *
from engine.conflicts import *
from engine.races import get_race
from engine.effects import get_effect, StackMode
from engine.aspects import get_aspect
from engine.inventory import Inventory
import engine.dice as dice
import engine.flags as flags

class EditStates(Enum):
    FULL = 1
    UPGRADE = 2
    LOCKED = 3

class Person(object):
    def __init__(self, name, level=0, race=None):
        self.name = name
        self.metadata = None
        self.inventory = Inventory()
        self.resources = Inventory()
        self.editable = EditStates.LOCKED #TODO: not enforced yet
        self.descriptors = []
        self.race = None

        #stats#
        self.abilities = OrderedDict()
        self.add_ability('Strength', 'physical', 10)
        self.add_ability('Endurance', 'physical', 10)
        self.add_ability('Dexterity', 'physical', 10)
        self.add_ability('Quickness', 'physical', 10)
        self.add_ability('Influence', 'social', 10)
        self.add_ability('Charisma', 'social', 10)
        self.add_ability('Empathy', 'social', 10)
        self.add_ability('Knowledge', 'mental', 10)
        self.add_ability('Perception', 'mental', 10)
        self.add_ability('Willpower', 'mental', 10)

        self.resistances = OrderedDict()
        self.add_resistance('Toughness', 'physical', [self.abl('Strength'), self.abl('Endurance')])
        self.add_resistance('Agility', 'physical', [self.abl('Dexterity'), self.abl('Quickness')])
        self.add_resistance('Courage', 'mental', [self.abl('Knowledge'), self.abl('Willpower')])
        self.add_resistance('Intuition', 'mental', [self.abl('Empathy'), self.abl('Perception')])
        self.add_resistance('Essence', 'magic', [])

        self.experience = OrderedDict()
        self.add_stat(self.experience, 'Level', level)
        self.add_stat(self.experience, 'Abilities', level + 100)
        self.add_stat(self.experience, 'Skills', level*3 + 150)
        self.add_stat(self.experience, 'Aspects', level + 20)
        self.add_stat(self.experience, 'Unbound', level*2)

        self.needs = OrderedDict()
        self.add_stat(self.needs, 'Fellowship', 0, -5, 5)
        self.add_stat(self.needs, 'Ego', 0, -5, 5)
        self.add_stat(self.needs, 'Adventure', 0, -5, 5)
        self.add_stat(self.needs, 'Wealth', 0, -5, 5)
        self.add_stat(self.needs, 'Sexuality', 0, -5, 5)
        self.add_stat(self.needs, 'Comfort', 0, -5, 5)

        self.skills = OrderedDict()
        self.add_skill('Climbing', ['physical'])
        self.add_skill('Swimming', ['physical'])
        self.add_skill('Riding', ['physical', 'social'])
        self.add_skill('Stealth', ['physical'])
        self.add_skill('Combat', ['physical'])
        self.add_skill('Shooting', ['physical', 'mental'])
        self.add_skill('Tactics', ['mental', 'social'])
        self.add_skill('Acrobatics', ['physical'])
        self.add_skill('Nautical', ['physical', 'mental'])
        self.add_skill('Crafts', ['physical', 'mental'])
        self.add_skill('Drawing', ['physical', 'mental'])
        self.add_skill('Cooking', ['physical', 'mental'])
        self.add_skill('Masquerade', ['physical', 'social'])
        self.add_skill('Music', ['physical', 'mental', 'social'])
        self.add_skill('Alchemy', ['mental'])
        self.add_skill('Healing', ['physical', 'mental'])
        self.add_skill('Meditation', ['mental'])
        self.add_skill('Maths', ['mental'])
        self.add_skill('Linguistics', ['mental', 'social'])
        self.add_skill('City Life', ['mental', 'social'])
        self.add_skill('Country Life', ['physical', 'mental', 'social'])
        self.add_skill('Survival', ['physical', 'mental'])
        self.add_skill('Search', ['mental'])
        self.add_skill('Teaching', ['mental', 'social'])
        self.add_skill('Negotiation', ['mental', 'social'])
        self.add_skill('Intimidation', ['mental', 'social'])
        self.add_skill('Charm', ['social'])
        self.add_skill('Subterfuge', ['physical', 'social'])
        self.add_skill('Resonance', ['mental', 'social'])
        self.add_skill('Spellcasting', ['mental', 'social'])

        self.nimbus = OrderedDict()

        self.aspects = OrderedDict()

        self.stats = [self.abilities, self.resistances, self.experience, self.needs, self.skills, self.nimbus, self.aspects]

        ### Transient data and state tracking ###
        self.effects = OrderedDict()
        self.combat_status = CombatStatus()

        self.exp_collected = 0
        self.exp_required = 100
        self.exp_step = 0

        self.hunger_level = Stat('Hunger', 0, 0, 100)

        ### Racial modifiers
        if race is not None:
            self.set_race(get_race(race))

        self.recalc() #set max values
        #self.default_modstats() #for test/debug

    def add_stat(self, container, name, value=0, minval=0, maxval=99999):
        container[name] = Stat(name, value, minval, maxval)

    def add_ability(self, name, scope, value=0):
        self.abilities[name] = Ability(name, scope, value)

    def add_resistance(self, name, scope, base_stats):
        self.resistances[name] = Resistance(name, scope, base_stats)

    def add_skill(self, name, scope, mod_stat=None, value=5):
        self.skills[name] = Skill(name, scope, mod_stat, value)

    def add_aspect(self, name, conviction, recalc=True):
        aspect = get_aspect(name)
        aspect.conviction = conviction
        self.aspects[aspect.name] = aspect

        for e in aspect.effects:
            self.add_effect(e, recalc)

    def remove_aspect(self, name, recalc=True):
        if name in self.aspects:
            for e in self.aspects[name].effects:
                self.remove_effect(e, recalc)

            del self.aspects[name]

    def add_effect(self, e, recalc=True):
        print('Adding effect: '+e.name)

        if e.name in self.effects: #handle duplicates
            if e.stackmode == StackMode.STACK:
                self.effects[e.name].strength += e.strength
            elif e.stackmode == StackMode.REPLACE:
                self.effects[e.name].strength = e.strength
            if e.stackmode == StackMode.REPLACE_HIGHER:
                if self.effects[e.name].strength < e.strength:
                    self.effects[e.name].strength = e.strength
            if e.stackmode == StackMode.DENY:
                pass
        else:
            self.effects[e.name] = e

        if recalc:
            self.recalc()

    def remove_effect(self, name, recalc=True):
        if name in self.effects:
            print('Removing effect: '+name)
            del self.effects[name]

            if recalc:
                self.recalc()

    def set_race(self, new_race):
        if self.race is not None: #remove old
            for d in self.race.descriptors:
                self.descriptors.remove(d)

            for a in self.race.aspects:
                self.remove_aspect(a)

        #add new
        for d in new_race.descriptors:
            if not d in self.descriptors:
                self.descriptors.append(d)

        for a in new_race.aspects:
            self.add_aspect(a, 'Race')

        self.race = new_race
        self.recalc()

    ### Access shorthands ###
    def abl(self, name=None):
        if name == None:
            return self.abilities.values()
        return self.abilities[name]

    def res(self, name=None):
        if name == None:
            return self.resistances.values()
        return self.resistances[name]

    def exp(self, name=None):
        if name == None:
            return self.experience.values()
        return self.experience[name]

    def ned(self, name=None):
        if name == None:
            return self.needs.values()
        return self.needs[name]

    def skl(self, name=None):
        if name == None:
            return self.skills.values()
        return self.skills[name]

    def nim(self, name=None):
        if name == None:
            return self.nimbus.values()
        return self.nimbus[name]

    ### Stat manipulation ###
    def recalc(self):
        level = self.experience['Level'].value

        for a in self.abilities.values():
            a.maxval = 20 + math.floor(level+1 / 2)
        for r in self.resistances.values():
            r.recalc()
        for s in self.skills.values():
            s.maxval = 10 + math.floor(level / 2)
            s.recalc()

        self.hook('recalc')

        #TODO: this deviates from the pen&paper rules - keep or change?
        self.combat_status.max_combat_penalty = max(self.skl(), key=lambda x:x.value).value

    def set(self, name, value):
        for col in self.stats:
            for k in col.keys():
                if k == name:
                    col[k].set(value)
                    if col == self.abilities:
                        self.recalc()
                    return

        raise KeyError('Stat "'+name+'" not found in character')

    def mod(self, name, mod):
        for col in self.stats:
            for k in col.keys():
                if k == name:
                    col[k].mod(mod)
                    if col == self.abilities:
                        self.recalc()
                    return

        raise KeyError('Stat "'+name+'" not found in character')

    def get(self, name):
        for col in self.stats:
            for k in col.keys():
                if k == name:
                    return col[k].value

        raise KeyError('Stat "'+name+'" not found in character')

    def stat(self, name):
        for col in self.stats:
            for k in col.keys():
                if k == name:
                    return col[k]

        raise KeyError('Stat "'+name+'" not found in character')

    def increase_stat(self, name, step=1):
        stat = self.stat(name)

        if stat.base + step > stat.maxval:
            return

        for pool in stat.pools.keys():
            if self.experience[pool].value >= stat.pools[pool]*step:
                self.experience[pool].mod(-1*stat.pools[pool]*step)
                stat.mod(step)
                self.recalc()
                return

    ### Stat use and conflicts ###
    def get_allowed_modstats(self, skill):
        result = []

        for abl in self.abl():
            if abl.scope in skill.scope:
                result.append(abl)

        return result

    def get_modstats_for_scope(self, scope):
        result = []

        for abl in self.abl():
            if abl.scope == scope:
                result.append(abl)

        return result

    def get_penalties(self, scope):
        total = 0
        for res in self.resistances.values():
            if res.scope == scope or scope == 'all' or (scope == 'combat' and (res.scope == 'mental' or res.scope == 'physical')):
                total += res.get_penalty()

        return total

    def roll(self, name, scope='none', mod=0, mode='default'):
        if flags.debug:
            print('Rolling '+name+'...')
            print('Stat value: '+str(self.get(name)))
            print('Modifier: '+str(mod))
            print('Penalty: '+str(self.get_penalties(scope)))

        mod -= self.get_penalties('combat')
        mod += self.combat_status.prepared_bonus
        self.combat_status.prepared_bonus = 0

        if name in self.inventory.tools:
            mod += self.inventory.tools[name].quality

        return dice.roll(self.get(name)+mod, mode)

    def roll_initiative(self):
        self.combat_status.initiative = self.roll('Quickness')

    def attack_severity(self, skill):
        if skill in self.inventory.tools:
            return self.inventory.tools[skill].damage_level

        return self.combat_status.attack_severity

    def find_defense_for_attack(self, skill_name):
        skill = self.skl(skill_name)
        stats = self.get_allowed_modstats(skill)

        try:
            stats.remove(skill.mod_stat)
        except ValueError:
            pass #nothing to do here

        return max(stats, key=lambda x:x.value).name

    def find_defense_for_block(self, scope):
        return max(self.get_modstats_for_scope(scope), key=lambda x:x.value).name

    def _calc_defense(self, stat, action, scope):
        self.combat_status.current_defense = self.get(stat) - 10
        self.combat_status.current_defense += self.hook('calc_defense', {'scope': scope, 'action': action})
        return self.combat_status.current_defense - self.get_penalties('combat')

    # Actions resolve as follows:
    # BLOCK: no attack, defense = ability modifier
    # WEAKEN: roll attack and defense
    # HURT: roll attack and defense, target adds ability modifier to defense
    # RECKLES: roll attack with d10, defender's defense = ability modifier
    def fight(self, profile, target_profile):
        if profile.action == CombatActions.BLOCK:
            return (-100, self._calc_defense(profile.defense, profile.action, target_profile.scope))
        else:
            mod = - min(self.combat_status.combat_penalty, self.get(profile.skill)) #TODO: workaround
            if target_profile.action == CombatActions.HURT or target_profile.action == CombatActions.RECKLESS:
                mod += self._calc_defense(profile.defense, profile.action, target_profile.scope)
            if profile.action == CombatActions.RECKLESS:
                mode = 'd10'
            else:
                mode = 'default'

            attack = self.roll(profile.skill, scope=profile.scope, mod=mod, mode=mode)

            if target_profile.action == CombatActions.RECKLESS:
                defense = self._calc_defense(profile.defense, profile.action, target_profile.scope)
            else:
                defense = attack

            return (attack, defense)

    def take_combat_penalty(self, skill, amount):
        old_value = self.combat_status.combat_penalty
        effective = max(self.get(skill) - self.combat_status.combat_penalty, 0)

        #print('Old value: '+str(old_value));
        #print('Skill: '+skill);
        #print('Reduce by: '+str(min(amount, effective)));
        #print('Max: '+str(self.combat_status.max_combat_penalty));

        self.combat_status.combat_penalty += min(amount, effective)
        if self.combat_status.combat_penalty > self.combat_status.max_combat_penalty:
            self.combat_status.combat_penalty = self.combat_status.max_combat_penalty

        difference = self.combat_status.combat_penalty - old_value
        #print('Taken: '+str(difference));

        if difference > 0:
            self.combat_status.weakened = True

        return difference

    def prepare_action(self, skill_name):
        self.combat_status.prepared_bonus = round(self.roll(skill_name, mode='d10') * 0.2)

        self.combat_status.prepared_bonus += self.hook('prepare', {'skill': skill_name})

        return self.combat_status.prepared_bonus

    def recover_action(self, skill_name, resistance, severity):
        stat = self.res(resistance)
        result = round(stat.get_damage() / 2)
        stat.heal(result, severity)
        return result

    def take_damage(self, resistance, amount, severity, tags=[]):
        if amount <= 0:
            return

        print(self.name+' takes '+str(amount)+' '+severity+' '+resistance+' damage.')

        #TODO: effect, aspect
        mod = self.hook('take_damage', {'resistance': resistance, 'amount': amount, 'severity': severity, 'tags': tags})

        if amount+mod <= 0:
            return

        self.res(resistance).damage(amount+mod, severity)

    def is_defeated(self):
        if self.res('Toughness').get_remaining() <= 0:
            return True

        return self.get_penalties('combat') >= 20

    ### Healing and recovery ###
    def heal(self, resistance, amount, severity):
        if amount <= 0:
            return

        #TODO: effect, aspect
        mod = self.hook('heal', {'resistance': resistance, 'amount': amount, 'severity': severity})

        if amount+mod <= 0:
            return

        self.res(resistance).heal(amount+mod, severity)

    def short_rest(self, hours):
        self.heal('Toughness', hours, 'L')

    def long_rest(self, food_level, comfort_level):
        self.short_rest(10)
        self.heal('Toughness', 1, 'M')

        self.hunger_level.mod(-food_level)
        self.take_damage('Toughness', self.hunger_level.value, 'L')

        amount = abs(comfort_level) + self.get('Comfort')
        if comfort_level > 0:
            self.heal('Courage', amount, 'M')
            self.heal('Intuition', amount, 'M')
        elif comfort_level < 0:
            self.take_damage('Courage', amount, 'M')
            self.take_damage('Intuition', amount, 'M')


    ### Effects ###
    def hook(self, key, params={}):
        result = 0

        for k in self.effects.keys():
            mod = self.effects[k].hook(key, self, params)
            if mod is not None:
                result += mod

        return result

    ### Experience system ###

    def gain_experience(self, amount):
        self.exp_collected += amount

        while self.exp_collected >= self.exp_required:
            self.exp_collected -= self.exp_required
            self.gain_exp_step()

    def gain_exp_step(self):
        self.exp_step += 1

        if self.exp_step >= 5: #gain a level and a skill point
            self.experience['Level'].mod(1)
            self.experience['Abilities'].mod(1)
            self.experience['Aspects'].mod(1)
            self.experience['Skills'].mod(1)
            self.exp_step = 0
        elif self.exp_step % 2 == 1: #odd: gain a skill point
            self.experience['Skills'].mod(1)
        else: #even: gain an unbound point
            self.experience['Unbound'].mod(1)

        self.recalc()

    ### Character creation and batch updates ###
    def default_modstats(self): #TODO: replace with data driven approach - maybe classes?
        self.skills['Climbing'].mod_stat = self.abl('Strength');
        self.skills['Swimming'].mod_stat = self.abl('Strength');
        self.skills['Riding'].mod_stat = self.abl('Endurance');
        self.skills['Stealth'].mod_stat = self.abl('Dexterity');
        self.skills['Combat'].mod_stat = self.abl('Strength');
        self.skills['Shooting'].mod_stat = self.abl('Perception');
        self.skills['Tactics'].mod_stat = self.abl('Charisma');
        self.skills['Acrobatics'].mod_stat = self.abl('Quickness');
        self.skills['Nautical'].mod_stat = self.abl('Endurance');
        self.skills['Crafts'].mod_stat = self.abl('Dexterity');
        self.skills['Drawing'].mod_stat = self.abl('Dexterity');
        self.skills['Cooking'].mod_stat = self.abl('Willpower');
        self.skills['Rope Use'].mod_stat = self.abl('Quickness');
        self.skills['Music'].mod_stat = self.abl('Charisma');
        self.skills['Alchemy'].mod_stat = self.abl('Knowledge');
        self.skills['Healing'].mod_stat = self.abl('Perception');
        self.skills['Meditation'].mod_stat = self.abl('Influence');
        self.skills['Maths'].mod_stat = self.abl('Knowledge');
        self.skills['Linguistics'].mod_stat = self.abl('Knowledge');
        self.skills['City Life'].mod_stat = self.abl('Empathy');
        self.skills['Country Life'].mod_stat = self.abl('Quickness');
        self.skills['Survival'].mod_stat = self.abl('Endurance');
        self.skills['Search'].mod_stat = self.abl('Perception');
        self.skills['Teaching'].mod_stat = self.abl('Influence');
        self.skills['Negotiation'].mod_stat = self.abl('Influence');
        self.skills['Intimidation'].mod_stat = self.abl('Willpower');
        self.skills['Charm'].mod_stat = self.abl('Charisma');
        self.skills['Subterfuge'].mod_stat = self.abl('Empathy');
        self.skills['Resonance'].mod_stat = self.abl('Empathy');
        self.skills['Spellcasting'].mod_stat = self.abl('Willpower');

    def batch_set(self, updates, maxPoints=None, step=0):
        total = 0
        for val in updates.values():
            total += int(val)

        if maxPoints is not None:
            if total > maxPoints:
                raise ValueError('Point limit of '+str(maxPoints)+' exceeded.')
            else:
                self.experience['Unbound'].mod((maxPoints - total)*step)

        for name in updates.keys():
            self.set(name, int(updates[name]))

    def batch_assign_modstats(self, updates):
        print('Assignging stats: '+str(updates));
        for key in updates.keys():
            self.skl(key).mod_stat = self.abl(updates[key])
