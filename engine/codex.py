codex_ready = False

co_stats = {}
co_creatures = {}
co_places = {}
co_items = {}

def set_entry(container, key, description):
    container[key.lower()] = description

def get_entry(container, key):
    global codex_ready
    if not codex_ready:
        init_codex()

    try:
        return container[key.lower()]
    except KeyError:
        return ''

def init_codex():
    global codex_ready
    codex_ready = True
    
    set_entry(co_stats, 'Strength',
    'Your ability to lift, carry and swing heavy objects. Affects Toughness.')
    set_entry(co_stats, 'Endurance',
    'Your stamina, health and general fortitude. Affects Toughness.')
    set_entry(co_stats, 'Dexterity',
    'Includes coordination, sense of balance and fine motor skills. Affects Agility.')
    set_entry(co_stats, 'Quickness',
    'Your reflexes, sprint speed and ability to dodge. Affects Agility.')
    set_entry(co_stats, 'Influence',
    'Your social standing, reputation and ability to lead. Affects Nimbus.')
    set_entry(co_stats, 'Charisma',
    'Your appearance and the ability to manipulate others. Affects Nimbus.')
    set_entry(co_stats, 'Empathy',
    'Your ability to read emotions and social cues. Affects Intuition.')
    set_entry(co_stats, 'Knowledge',
    'Includes logical thinking, long term memory and education. Affects Courage.')
    set_entry(co_stats, 'Perception',
    'Sharpness of senses and short term memory. Affects Intuition.')
    set_entry(co_stats, 'Willpower',
    'Patience and the ability to concentrate and resist being manipulated. Affects Courage.')
