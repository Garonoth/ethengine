from enum import Enum
from collections import OrderedDict

from engine.conflicts import CombatActions

effects_ready = False
stock_effects = {}

class StackMode(Enum):
    STACK = 1
    REPLACE = 2
    REPLACE_HIGHER = 3
    DENY = 4

class Visibility(Enum):
    HIDDEN = 1
    ASPECT = 2
    CONSUMABLE = 3
    VISIBLE = 4

class Rule(object):
    def __init__(self, callback, params = {}):
        self._prepare(callback, params)

    def _prepare(self, callback, params):
        self.callback = callback
        self.params = params

class Effect(object):
    def __init__(self, name, stackmode, visibility, strength = 1):
        self._prepare(name, stackmode, visibility, strength)
        self.rules = OrderedDict()
        self.conditions = OrderedDict()

    def _prepare(self, name, stackmode, visibility, strength):
        self.name = name
        self.stackmode = stackmode
        self.visibility = visibility
        self.strength = strength

    def hook(self, hook, char, hook_params={}):
        for h in self.rules.keys():
            if h == hook:
                rule = self.rules[h]
                return rule.callback(self, char, {**rule.params, **hook_params}, self.strength)

class Callback(object):
    @staticmethod
    def modify_stat(effect, char, params, power):
        char.stat(params['stat']).value += power

    @staticmethod
    def gain_experience(effect, char, params, power):
        char.gain_experience(power * params['amount'])

    @staticmethod #returns: modifier
    def bonus_to_prepare(effect, char, params, power):
        if params['skill'] in params['affected_skills']:
            return power

        return 0

    @staticmethod #returns: modifier
    def modify_defense(effect, char, params, power):
        try:
            if params['scope'] in params['affected_scopes']:
                return power
        except KeyError: pass

        try:
            if params['action'] in params['affected_actions']:
                return power
        except KeyError: pass

        return 0

    @staticmethod #returns: modifier
    def double_healing(effect, char, params, power):
        if params['resistance'] in params['affected_resistances']:
            return params['amount']

        return 0

    @staticmethod
    def heal(effect, char, params, power):
        if 'quality' in params:
            power += params['quality']

        if power > 0:
            char.heal(params['stat'], power, params['severity'])

def add_effect(e):
    global effects_ready
    global stock_effects

    if not effects_ready:
        init_effects()

    stock_effects[e.name] = e

def get_effect(name):
    global effects_ready
    global stock_effects

    if not effects_ready:
        init_effects()

    return stock_effects[name]

def init_effects():
    global effects_ready
    global stock_effects

    effects_ready = True
    stock_effects = {}

    ### Aspect effects ###

    e = Effect('Fearless', StackMode.DENY, Visibility.ASPECT, 4)
    e.rules['recalc'] = Rule(Callback.modify_stat, {'stat': 'Courage'})
    add_effect(e)

    e = Effect('Clearheaded', StackMode.DENY, Visibility.ASPECT, 4)
    e.rules['recalc'] = Rule(Callback.modify_stat, {'stat': 'Intuition'})
    add_effect(e)

    e = Effect('Tough', StackMode.DENY, Visibility.ASPECT, 4)
    e.rules['recalc'] = Rule(Callback.modify_stat, {'stat': 'Toughness'})
    add_effect(e)

    e = Effect('Nimble', StackMode.DENY, Visibility.ASPECT, 4)
    e.rules['recalc'] = Rule(Callback.modify_stat, {'stat': 'Agility'})
    add_effect(e)

    e = Effect('Magic Preparation Bonus', StackMode.DENY, Visibility.ASPECT, 3)
    e.rules['prepare'] = Rule(Callback.bonus_to_prepare, {'affected_skills': ['Spellcasting']})
    add_effect(e)

    e = Effect('Social Preparation Bonus', StackMode.DENY, Visibility.ASPECT, 3)
    e.rules['prepare'] = Rule(Callback.bonus_to_prepare, {'affected_skills': ['Teaching', 'Negotiation', 'Intimidation']})
    add_effect(e)

    e = Effect('Rogue Preparation Bonus', StackMode.DENY, Visibility.ASPECT, 3)
    e.rules['prepare'] = Rule(Callback.bonus_to_prepare, {'affected_skills': ['Stealth', 'Acrobatics', 'Charm', 'Subterfuge']})
    add_effect(e)

    e = Effect('Physical Defense', StackMode.DENY, Visibility.ASPECT, 2)
    e.rules['calc_defense'] = Rule(Callback.modify_defense, {'affected_scopes': ['physical']})
    add_effect(e)

    e = Effect('Mental Defense', StackMode.DENY, Visibility.ASPECT, 2)
    e.rules['calc_defense'] = Rule(Callback.modify_defense, {'affected_scopes': ['mental']})
    add_effect(e)

    e = Effect('Block Defense', StackMode.DENY, Visibility.ASPECT, 4)
    e.rules['calc_defense'] = Rule(Callback.modify_defense, {'affected_actions': [CombatActions.BLOCK]})
    add_effect(e)

    e = Effect('Double Healing', StackMode.DENY, Visibility.ASPECT)
    e.rules['heal'] = Rule(Callback.double_healing, {'affected_resistances': ['Toughness']})
    add_effect(e)

    ### Consumable effects ###

    e = Effect('Healing Salve', StackMode.STACK, Visibility.CONSUMABLE, 1)
    e.rules['consume'] = Rule(Callback.heal, {'stat': 'Toughness', 'severity': 'L'})
    add_effect(e)

    e = Effect('Healing Draught', StackMode.STACK, Visibility.CONSUMABLE, 1)
    e.rules['consume'] = Rule(Callback.heal, {'stat': 'Toughness', 'severity': 'M'})
    add_effect(e)
