from collections import OrderedDict

class Stat(object):
    def __init__(self, name, value, minval = 0, maxval = 99999):
        self._prepare(name, value, minval, maxval)
        self.pools = OrderedDict()

    def _prepare(self, name, value, minval, maxval):
        self.name = name
        self.value = value
        self.base = value
        self.minval = minval
        self.maxval = maxval

    def _calc(self):
        if self.base < self.minval:
            self.base = self.minval
        elif self.base > self.maxval:
            self.base = self.maxval

        self.value = self.base

    def recalc(self):
        self._calc()

    def set(self, value):
        self.base = value
        self.recalc()

    def mod(self, mod):
        self.base += mod
        self.recalc()

    def cost(self, pool):
        if pool in self.pools.keys():
            return self.pools[pool]

        return 0

    def __str__(self):
        return '<Stat: '+self.name+' '+str(self.value)+'>'

class Ability(Stat):
    def __init__(self, name, scope, value, minval = 1, maxval = 99999):
        self._prepare(name, value, minval, maxval)
        self.scope = scope
        self.pools = OrderedDict([('Abilities', 1), ('Unbound', 5)])

    def __str__(self):
        return '<Ability: '+self.name+' '+str(self.value)+'>'

class Resistance(Stat):
    def __init__(self, name, scope, base_stats, value=0, minval = 0, maxval = 99999):
        self._prepare(name, value, minval, maxval)
        self.scope = scope
        self.base_stats = base_stats
        self.pools = OrderedDict()

        self.light = 0
        self.medium = 0
        self.severe = 0
        self.recalc()

    def recalc(self):
        self._calc()
        for stat in self.base_stats:
            self.value += stat.value

        return self.value

    def damage(self, amount, severity):
        if severity == 'severe' or severity == 'S':
            self.severe += amount
            if self.severe > self.value:
                self.severe = self.value
        elif severity == 'medium' or severity == 'M':
            if self.medium + amount <= self.value:
                self.medium += amount
            else:
                amount -= (self.value - self.medium)
                self.medium = self.value
                self.damage(amount, 'S') #spill over
        else:
            if self.light + amount <= self.value:
                self.light += amount
            else:
                amount -= (self.value - self.light)
                self.light = self.value
                self.damage(amount, 'M') #spill over

    def heal(self, amount, severity):
        if severity == 'light' or severity == 'L':
            self.light -= amount
            if self.light < 0:
                self.light = 0
        elif severity == 'medium' or severity == 'M':
            if self.medium - amount >= 0:
                self.medium -= amount
            else:
                amount -= self.medium
                self.medium = 0
                self.heal(amount, 'L') #spill over
        else:
            if self.severe - amount >= 0:
                self.severe -= amount
            else:
                amount -= self.severe
                self.severe = 0
                self.heal(amount, 'M') #spill over

    def get_damage(self):
        return min(self.light + self.medium + self.severe, self.value)

    def get_penalty(self):
        return max(10-self.get_remaining(), 0)

    def get_remaining(self):
        return self.value - self.get_damage()

    def __str__(self):
        return '<Resistance: %s %d (%d/%d/%d)>' % (self.name, self.value, self.light, self.medium, self.severe)

class Skill(Stat):
    def __init__(self, name, scope, mod_stat, value, minval = 0, maxval = 99999):
        self._prepare(name, value, minval, maxval)
        self.scope = scope
        self.mod_stat = mod_stat
        self.pools = OrderedDict([('Skills', 1), ('Unbound', 1)])

        self.recalc()

    def recalc(self):
        self._calc()
        if self.mod_stat != None:
            self.value += self.mod_stat.value - 10

    def __str__(self):
        return '<Skill: %s %d (base %d)>' % (self.name, self.value, self.base)

class Aspect(Stat):
    def __init__(self, name, cost, effects, minval = 1, maxval = 99999):
        self._prepare(name, cost, minval, maxval)
        self.conviction = None
        self.effects = effects
        self.pools = OrderedDict([('Aspects', cost), ('Unbound', cost)])
