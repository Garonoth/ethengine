from collections import OrderedDict
from enum import Enum

class ItemCategories(Enum):
    CLUTTER = 0
    CURRENCY = 1
    FOOD = 2
    MEDICINE = 3
    TOOLS = 4
    WEARABLE = 5
    MATERIALS = 6

class WearableAreas(Enum):
    HEAD = 0
    CHEST = 1
    GROIN = 2
    LEFT_ARM = 3
    RIGHT_ARM = 4
    HANDS = 5
    LEFT_LEG = 6
    RIGHT_LEG = 7
    FEET = 8

class Item(object):
    def __init__(self, name, category, weight=0, base_value=0, quality=0, min_quality=-5, effects=[],  description='', icon='bag.png', \
    unique=False, quality_levels=None):
        if quality_levels is None:
            self.quality_levels = ['Ruined', 'Damaged', 'Rough', '', 'Fine', 'Pristine', 'Exceptional', 'Priceless']
        else:
            self.quality_levels = quality_level

        self._prepare(name, category, weight, base_value, quality, min_quality, effects, description, icon, unique, False)

    def _prepare(self, name, category, weight, base_value, quality, min_quality, effects, description, icon, unique, can_spoil):
        self.name = name
        self.birth_name = name #remember for convenience
        self.category = category
        self.weight = weight
        self.base_value = base_value
        self.quality = quality
        self.min_quality = min_quality
        self.effects = effects
        self.description = description
        self.icon = icon
        self.unique = unique
        self.can_spoil = can_spoil
        self.spoiling_quality = 0
        self.stack_size = 1
        self.base_item = None
        self.calc_value()

    def calc_value(self):
        if self.quality < 0:
            self.value = self.base_value / (abs(self.quality)+1)
        else:
            self.value = self.base_value * (self.quality+1)

        prefix = self.prefix_for_quality()
        if len(prefix) > 0:
            self.name = prefix+' '+self.birth_name
        else:
            self.name = self.birth_name

    def prefix_for_quality(self):
        if self.quality <= -5:
            return self.quality_levels[0]
        elif self.quality <= -3:
            return self.quality_levels[1]
        elif self.quality <= -1:
            return self.quality_levels[2]
        elif self.quality == 0:
            return self.quality_levels[3]
        elif self.quality <= 2:
            return self.quality_levels[4]
        elif self.quality <= 4:
            return self.quality_levels[5]
        elif self.quality <= 6:
            return self.quality_levels[6]
        else:
            return self.quality_levels[7]

    def spoil(self):
        if self.quality > spoiling_quality:
            self.quality = spoiling_quality
        elif self.quality > self.min_quality:
            self.quality -= 1

        self.calc_value()

    def stacks_with(self, other):
        if other.birth_name != self.birth_name:
            return False
        if other.base_item != self.base_item:
            return False
        if other.quality != self.quality:
            return False

        return True

    def __str__(self):
        return '<Item: '+str(self.stack_size)+'x '+self.name+' q'+str(self.quality)+'>'

class Tool(Item):
    def __init__(self, name, category, skills, damage_level='M', weight=0, base_value=0, quality=0, min_quality=-5, effects=[], \
    description='', icon='bag.png', unique=False, quality_levels=None):
        if quality_levels is None:
            self.quality_levels = ['Primitive', 'Crude', 'Chipped', '', 'Sturdy', 'Fine', 'Excellent', 'Stellar']
        else:
            self.quality_levels = quality_levels

        self._prepare(name, category, weight, base_value, quality, min_quality, effects, description, icon, unique, False)
        self.skills = skills
        self.damage_level = damage_level

class Consumable(Item):
    def __init__(self, name, category, weight=0, base_value=0, quality=0, min_quality=-5, effects=[], description='', icon='bag.png', \
    unique=False, quality_levels=None):
        if quality_levels is None:
            self.quality_levels = ['Spoiled', 'Stale', 'Weak', '', 'Good', 'Potent', 'Exquisite', 'Supreme']
        else:
            self.quality_levels = quality_levels

        self._prepare(name, category, weight, base_value, quality, min_quality, effects, description, icon, unique, True)

    def consumed(self, char):
        params = {'quality': self.quality}
        for e in self.effects:
            e.hook('consume', char, params)

class Wearable(Item):
    def __init__(self, name, category, areas, weight=0, base_value=0, quality=0, min_quality=-5, effects=[], description='', icon='bag.png', \
    unique=False, quality_levels=None):
        if quality_levels is None:
            self.quality_levels = ['Battered', 'Shabby', 'Worn', '', 'Elegant', 'Ornate', 'Splendid', 'Magnificent']
        else:
            self.quality_levels = quality_levels

        self._prepare(name, category, weight, base_value, quality, min_quality, effects, description, icon, unique, False)
        self.areas = areas

class Inventory(object):
    def __init__(self):
        self.money = 0
        self.items = []
        self.tools = {}

    def add(self, item):
        for i in self.items:
            if i.stacks_with(item):
                i.stack_size += item.stack_size
                return

        self.items.append(item)

    def equip(self, item):
        for skill in item.skills:
            if not (skill in self.tools) or self.tools[skill] is None or self.tools[skill].quality < item.quality:
                self.tools[skill] = item

    def unequip(self, item):
        for skill in item.skills:
            if self.tools[skill] == item:
                del self.tools[skill]

    def remove(self, item):
        self.unequip(item)

        for i in self.items:
            if i.stacks_with(item):
                if i.stack_size > item.stack_size:
                    i.stack_size -= item.stack_size
                    return

        self.items.remove(item)
