from enum import Enum

class CombatActions(Enum):
    BLOCK = 1
    WEAKEN = 2
    HURT = 3
    RECKLESS = 4
    PREPARE = 5
    RECOVER = 6

class CombatProfile(object):
    def __init__(self, name, action, skill, defense, target_resistance, severity, scope, callbacks):
        self.name = name
        self.action = action
        self.skill = skill
        self.defense = defense
        self.target_resistance = target_resistance
        self.severity = severity
        self.scope = scope
        self.success = callbacks[0]
        self.tie = callbacks[1]
        self.failure = callbacks[2]

class CombatStatus(object):
    def __init__(self):
        self.team = 0
        self.initiative = 0
        self.combat_penalty = 0
        self.max_combat_penalty = 5
        self.weakened = False
        self.prepared_bonus = 0
        self.current_defense = 0
        self.last_target_index = -1
        self.attack_severity = 'L'
