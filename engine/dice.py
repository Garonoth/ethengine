import random

import engine.flags as flags

def d20():
    return random.randint(1, 20)

def d10(): #technically d10-1
    return random.randint(0, 9)

def roll(value, mode='default'):
    if mode == 'd10': #"advantage" mode: roll d10 instead of d20
        rn = d10()
        if rn == 1:
            rn -= d10()
    elif mode == 'crit': #auto-crit: assume a 1 on d20
        rn = 1-d10()
    elif mode == 'default': #roll d20, add or subtract d10 for crit
        rn = d20()
        if rn == 1:
            rn -= d10()
        elif rn == 20:
            rn += d10()
    else:
        raise ValueError('Roll mode "'+mode+'" not recognized')

    if flags.debug:
        print('Eff. value: '+str(value))
        print('Rolled: '+str(rn))
        print('Result: '+str(value - rn))

    return value - rn
